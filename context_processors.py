import os

def local_style(request):
    return {
        'local_css': os.environ.get("LOCAL_CSS"),
    }
