from __future__ import unicode_literals

from django.apps import AppConfig


class LocalStyleConfig(AppConfig):
    name = 'local_style'
